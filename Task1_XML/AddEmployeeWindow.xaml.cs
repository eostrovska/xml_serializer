﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using Task1_XML.Model;

namespace Task1_XML
{
    /// <summary>
    /// Interaction logic for AddEmployeeWindow.xaml
    /// </summary>
    public partial class AddEmployeeWindow : Window
    {
        public AddEmployeeWindow()
        {
            InitializeComponent();
        }

        private void CreateEmployee_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var employeesFromFile = new List<Employee>();

                XmlSerializer formatter = new XmlSerializer(typeof(Employee[]));

                using (FileStream fs = new FileStream(ConfigurationManager.AppSettings["EmployeeDataFilePath"], FileMode.OpenOrCreate))
                {
                    Employee[] newEmployees = (Employee[])formatter.Deserialize(fs);

                    employeesFromFile.AddRange(newEmployees);
                }

                XmlSerializer singleFormatter = new XmlSerializer(typeof(Employee));
                var id = employeesFromFile.LastOrDefault().Id + 1;

                var newEmployee = new Employee(NameTextBox.Text, id, float.Parse(SalaryTextBox.Text), PositionTextBox.Text);

                var newEmployeeString = @"<Employee>"+
                                            "<Id>" + newEmployee.Id + "</Id>" +
                                            "<Name>" + newEmployee.Name + "</Name>" +
                                            "<Salary>" + newEmployee.Salary + "</Salary>" +
                                            "<Position>" + newEmployee.Position + "</Position>" +
                                       "</Employee>";

                XmlDocument doc = new XmlDocument();
                doc.Load(ConfigurationManager.AppSettings["EmployeeDataFilePath"]);
                XPathNavigator navigator = doc.CreateNavigator();
                navigator.MoveToChild("ArrayOfEmployee", "");
                navigator.AppendChild(newEmployeeString);

                doc.Save(ConfigurationManager.AppSettings["EmployeeDataFilePath"]);

                MessageBox.Show("Employee Added Successfully");

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Salary " + ex.Message);
            }
        }
    }
}
