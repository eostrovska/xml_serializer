﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1_XML.Model
{
    public class Employee
    {
        private int id;
        private string name;
        private float salary;
        private string position;

        public int Id
        {
            set { id = value; }
            get { return id; }
        }

        public string Name
        {
            set { name = value; }
            get { return name; }
        }

        public float Salary
        {
            set { salary = value; }
            get { return salary; }
        }

        public string Position
        {
            set { position = value; }
            get { return position; }
        }

        public Employee() { }

        public Employee(string name, int id, float salary, string position)
        {
            this.name = name;
            this.id = id;
            this.salary = salary;
            this.position = position;
        }
    }
}
