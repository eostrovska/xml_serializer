﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;
using System.Configuration;
using Task1_XML.Model;

namespace Task1_XML
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            string curFile = @"c:\temp\test.txt";
            Console.WriteLine(File.Exists(curFile) ? "File exists." : "File does not exist.");

            if (!File.Exists(ConfigurationManager.AppSettings["EmployeeDataFilePath"]))
            {
                List<Employee> employees = new List<Employee>
                                             {
                                                 new Employee("Kate", 1, 1200, "Manager"),
                                                 new Employee("Nik", 2, 1600, "ManagerOfSales"),
                                                 new Employee("Kate", 3, 1700, "Sales"),
                                                 new Employee("Kate", 4, 1300, "OfficeManager"),
                                                 new Employee("Kate", 5, 1900, "HeadOFSalesDepartment")
                                             };

                XmlSerializer formatter = new XmlSerializer(typeof(List<Employee>));

                using (FileStream fs = new FileStream(ConfigurationManager.AppSettings["EmployeeDataFilePath"], FileMode.OpenOrCreate))
                {
                    formatter.Serialize(fs, employees);
                }
            }
        }

        private void AddEmployee_Click(object sender, RoutedEventArgs e)
        {
            AddEmployeeWindow win2 = new AddEmployeeWindow();
            win2.Show();
        }

        private void FindEmployee_Click(object sender, RoutedEventArgs e)
        {
            if (NameTextBox.Text == String.Empty || PositionTextBox.Text == String.Empty)
            {
                MessageBox.Show("Name and Position can't be empty");

            }
            else
            {
                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(Employee[]));

                    var employeeFromFile = new List<Employee>();


                    using (FileStream fs = new FileStream(ConfigurationManager.AppSettings["EmployeeDataFilePath"], FileMode.OpenOrCreate))
                    {
                        Employee[] newEmployees = (Employee[])formatter.Deserialize(fs);

                        employeeFromFile.AddRange(newEmployees);
                    }

                    var foundEmployee = employeeFromFile.FirstOrDefault(x => x.Name == NameTextBox.Text && x.Position == PositionTextBox.Text);

                    var foundEmployeeWindow = new FindEmployeeResultWindow(foundEmployee);

                    foundEmployeeWindow.Show();
                }
                catch
                {
                    MessageBox.Show("Employee doesn't exist");
                }
            }
        }
    }
}
