﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;
using Task1_XML.Model;

namespace Task1_XML
{
    /// <summary>
    /// Interaction logic for FindEmployeeResultWindow.xaml
    /// </summary>
    public partial class FindEmployeeResultWindow : Window
    {
        public FindEmployeeResultWindow(Employee foundEmployee)
        {
            InitializeComponent();

            FoundEmployeeName.Content = foundEmployee.Name;
            FoundEmployeePosition.Content = foundEmployee.Position;
            FoundEmployeeSalary.Content = foundEmployee.Salary;
            FoundEmployeeId.Content = foundEmployee.Id;
        }
    }
}
